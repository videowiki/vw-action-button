
## Videowiki Action Button web component
`<vw-action-button></vw-action-button>`

### What does this component do
This component encapsulates most of the workflow in Videowiki, the button has 3 states namely `Transcribe`, `Proofread` and `Translate`

In the `Transcribe` state the button will upload the video to Videowiki's organization and initiate transcribing it. once transcribing is done the button will move to `Proofread` state.

In the `Proofread` state, clicking on the button should direct the user to the page in which he'll be able to proofread the transcription ( using the `<vw-proofread />` micro-app [link here](https://gitlab.com/videowiki/vw-proofread)) and perform any required edits. Once edits are done the button moves to the `Translate` state

In the `Translate` state, Clicking on the button enables the user to choose the language he want's to translate the video in, then directs him to the page in which he'll do the translation ( text edits, audio recordings, generating subtitles and exporting the translation ) using the `<vw-translate />` micro-app [link here](https://gitlab.com/videowiki/vw-translate)


### Usage:
##### Include the following script in you page
`<script src="https://videowiki-microapps.s3-eu-west-1.amazonaws.com/vw-action-button/v1.0.0.js" async />`

##### Use the web component `vw-action-button` anywhere on the page with the following properties
```
<vw-action-button
	apiRoot=''
	apiKey=''

	videoTitle=''
	videoURL=''

	proofreadURL=''
	proofreadQueryParamKey=''

	translationURL=''
	translationQueryParamKey=''
>
</vw-action-button>
```

### Properties

- apiRoot: The API to which the component will communicate, for integration with videowiki's original API use https://api.videowiki.org/api

- apiKey: Obtain an API key for your organization from the dashboard either from your videowiki instance or from www.videowiki.org if you're integrating with our api
- videoTitle: The title of the video ( this field will be used to upload the video if it's not uploaded to your organization in videowiki )
- videoURL: the URL where the video is hosted ( this field will be used to upload the video if it's not uploaded to you organization in videowiki )
- proofreadURL: the URL in which the user should be directed to perform proofreading. on this link the micro-app `vw-proofread` should be implemented to enable the user to proofread the video, example: [https://videowiki.videowiki.org/convert)
- proofreadQueryParamKey: the query parameter key that will have the id of the generated video. when the video is uploaded it gets an `_id` field that is required by the `vw-proofread` micro-app to know which video to proofread. when directing to the proofread page this component ( `vw-action-button` ) will expose the `_id` of the generated video via a query parameter in the URL named using this property. example: using `video` as the query param and `https://videowiki.videowiki.org/convert` as the proofreadURL will direct the user to `https://videowiki.videowiki.org/convert?video={VIDEO_ID_GETS_HERE}`

- translationURL: the URL in which the user should be directed to perform translation. on this link the micro-app `vw-translate` should be implemented to enable the user to translate the video, example [https://videowiki.videowiki.org/translation/article/](https://videowiki.videowiki.org/translation/article)
- translationQueryParamKey: the query parameter key that will have the id of the generated translation article. when a translation is started ( generated ) it gets an `_id` field that is required by the `vw-translate` micro-app to know which article translation to translate. when directing to the translation page this component ( `vw-action-button` ) will expose the `_id` of the generate article via a query param in the URL named using this property. example: using `article` as the query param and [https://videowiki.videowiki.org/translation/article](https://videowiki.videowiki.org/translation/article) as the translationURL will direct the user to `https://videowiki.videowiki.org/translation/article?article={ARTICLE_ID_GETS_HERE}`
- 