import React from 'react';
import { connect } from 'react-redux';

import * as actions from './modules/actions';

import Transcribe from './containers/Transcribe';

import Proofread from './containers/Proofread';
import Translate from './containers/Translate';

class ActionButton extends React.Component {

    state = {
    }

    componentWillMount = () => {
        this.intervalId = null;
        this.props.fetchVideoByTitle({ title: this.props.videoTitle, organization: this.props.organization._id });
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.videoTitle !== this.props.videoTitle) {
            this.props.fetchVideoByTitle({ title: nextProps.videoTitle, organization: this.props.organization._id })
        }
        // If the video is in converting stage, start a poller update each 10 seconds till it's done
        if (nextProps.video && nextProps.video.status === 'converting' && !this.intervalId) {
            this.intervalId = setInterval(() => {
                this.props.fetchVideoByTitle({ title: nextProps.videoTitle, organization: this.props.organization._id })
            }, 10 * 1000);
        }
        // If the video was in converting and became done, stop the poller
        if (this.intervalId && nextProps.video && ['done', 'failed'].indexOf(nextProps.video.status) !== -1) {
            clearInterval(this.intervalId)
            this.intervalId = null;
        }
        // if (this.props.video && nextProps.video && )
    }


    renderTranslate = () => {
        return <Translate videoTitle={this.props.videoTitle} translationURL={this.props.translationURL} translationQueryParamKey={this.props.translationQueryParamKey} />
    }

    renderProofread = () => {
        return <Proofread proofreadURL={this.props.proofreadURL} proofreadQueryParamKey={this.props.proofreadQueryParamKey} />
    }
    renderTranscribe = () => {
        return <Transcribe videoURL={this.props.videoURL} videoTitle={this.props.videoTitle} />
    }

    render() {
        const { user, organization, video } = this.props;

        if (!user || !organization) return <span>Loading...</span>
        if (!video || ['transcriping', 'cutting'].indexOf(video.status) !== -1) return this.renderTranscribe();
        if (['proofreading', 'converting'].indexOf(video.status) !== -1) return this.renderProofread()

        if (video.status === 'done') return this.renderTranslate();
        // Fall back to transcribe
        return this.renderTranscribe();
    }
}

const mapStateToProps = ({ actionButton }) => ({
    organization: actionButton.organization,
    user: actionButton.user,
    video: actionButton.video,
})

const mapDispatchToProps = (dispatch) => ({
    fetchVideoByTitle: (params) => dispatch(actions.fetchVideoByTitle(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(ActionButton)
