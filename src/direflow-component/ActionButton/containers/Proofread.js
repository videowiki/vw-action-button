import React from 'react';
import { connect } from 'react-redux';
import { Button } from 'semantic-ui-react';


class Proofread extends React.Component {

    render() {
        let { proofreadURL, proofreadQueryParamKey } = this.props;
        const extraContent = `${proofreadQueryParamKey || 'video'}=${this.props.video._id}`
        if (proofreadURL.indexOf('?') === -1) {
            proofreadURL += `?${extraContent}`;
        } else {
            proofreadURL += `&${extraContent}`;
        }
        const { video } = this.props;
        const loading = video && video.status === 'converting';
        return (
            <a
                href={proofreadURL}
            >
                <Button
                    primary
                    loading={loading}
                    disabled={loading}
                >
                    Proofread
                </Button>
            </a>
        )
    }
}


const mapStateToProps = ({ actionButton }) => ({
    organization: actionButton.organization,
    user: actionButton.user,
    video: actionButton.video,
})

export default connect(mapStateToProps)(Proofread);