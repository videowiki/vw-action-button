import React from 'react';
import { connect } from 'react-redux';
import { Button, Modal } from 'semantic-ui-react';

import SingleUpload from '../components/SingleUpload';

import { supportedLangs, isoLangsArray } from '../constants/langs';
import * as actions from '../modules/actions';

const speakersOptions = Array.apply(null, { length: 10 }).map(Number.call, Number).map((a, index) => ({ value: index + 1, text: index + 1 }));
let langsToUse = supportedLangs.map((l) => ({ ...l, supported: true })).concat(isoLangsArray.filter((l) => supportedLangs.every((l2) => l2.code.indexOf(l.code) === -1)))

const langsOptions = langsToUse.map((lang) => ({ key: lang.code, value: lang.code, text: `${lang.name} ( ${lang.code} ) ${lang.supported ? ' < Auto >' : ''}` }));

const DEFAULT_LANG_CODE = 'en-US';

class Transcribe extends React.Component {

    isSingleFormValid = () => {
        const { uploadVideoForm } = this.props;
        const { numberOfSpeakers, langCode } = uploadVideoForm;
        if (!numberOfSpeakers || !langCode) return false;
        return true;
    }

    onSingleUploadFormChange = (changes) => {
        let { uploadVideoForm } = this.props;
        Object.keys(changes).forEach((key) => {
            uploadVideoForm[key] = changes[key];
        })
        this.props.setUploadVideoForm({ ...uploadVideoForm });
    }

    onSubmit = () => {
        const { uploadVideoForm } = this.props;
        const { videoTitle, videoURL } = this.props;
        const formValues = {
            ...uploadVideoForm,
            url: videoURL,
            title: videoTitle,
            organization: this.props.organization._id,
        }
        // console.log(formValues)
        this.props.uploadAndTranscribeVideo(formValues);
    }

    onTranscribeClick = () => {
        this.props.setUploadVideoModalOpen(true)
    }

    renderUploadVideoModal = () => {
        if (!this.props.uploadVideoModalOpen) return null;
        const disabled = !(this.isSingleFormValid() && !this.props.loading);
        return (
            <Modal
                open={this.props.uploadVideoModalOpen}
                onClose={() => this.props.setUploadVideoModalOpen(false)}
            >
                <Modal.Header
                    style={{ backgroundColor: '#2185d0', color: 'white' }}
                >
                    Transcribe Video
                </Modal.Header>
                <Modal.Content style={{ padding: 0 }}>
                    <SingleUpload
                        {...this.props}
                        disabled={disabled}
                        onChange={this.onSingleUploadFormChange}
                        value={{ ...this.props.uploadVideoForm, videoURL: this.props.videoURL } || {}}
                        onSubmit={this.onSubmit}
                        langsOptions={langsOptions}
                        speakersOptions={speakersOptions}

                    />
                </Modal.Content>
                <Modal.Actions>

                    <Button
                        circular
                        style={{ paddingLeft: '3rem', paddingRight: '3rem' }}
                        onClick={() => this.props.setUploadVideoModalOpen(false)}
                        size={'large'}
                    >
                        Cancel
                    </Button>
                    <Button
                        circular
                        onClick={this.onSubmit}
                        disabled={disabled}
                        loading={this.props.loading}
                        primary
                        style={{ paddingLeft: '3rem', paddingRight: '3rem' }}
                        size={'large'}
                    >
                        Transcribe
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }

    render() {
        const { video } = this.props;
        const loading = (video && ['transcriping', 'cutting'].indexOf(video.status) !== -1) || this.props.loading;

        return (
            <span>

                <Button
                    disabled={loading}
                    loading={loading}
                    primary
                    onClick={this.onTranscribeClick}
                >
                    Transcribe
                </Button>
                {this.renderUploadVideoModal()}
            </span>
        )
    }
}


const mapStateToProps = ({ actionButton }) => ({
    organization: actionButton.organization,
    user: actionButton.user,
    video: actionButton.video,
    loading: actionButton.loading,
    uploadVideoForm: actionButton.uploadVideoForm,
    uploadVideoModalOpen: actionButton.uploadVideoModalOpen,
})

const mapDispatchToProps = (dispatch) => ({
    uploadAndTranscribeVideo: (form) => dispatch(actions.uploadAndTranscribeVideo(form)),
    setUploadVideoModalOpen: open => dispatch(actions.setUploadVideoModalOpen(open)),
    setUploadVideoForm: form => dispatch(actions.setUploadVideoForm(form)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Transcribe);