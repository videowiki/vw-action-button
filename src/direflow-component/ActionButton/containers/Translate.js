import React from 'react';
import { connect } from 'react-redux';
import { Button, Icon } from 'semantic-ui-react';
import AddHumanVoiceModal from '../components/AddHumanVoiceModal';
import { generateTranslatableArticle } from '../modules/actions';


class Translate extends React.Component {

    state = {
        modalOpen: false,
    }

    onSubmit = (langCode, langName) => {
        console.log('On SUBMIT', langCode, langName);
        const { video } = this.props;
        this.props.generateTranslatableArticle(this.props.translationURL, this.props.translationQueryParamKey, video.article, langCode, langName);
    }

    renderAddHumanvoiceModal = () => {

        const {
            translationURL,
            translationQueryParamKey
        } = this.props;

        return (
            <AddHumanVoiceModal
                translationURL={translationURL}
                translationQueryParamKey={translationQueryParamKey}
                translatedArticles={this.props.translatedArticles}
                open={this.state.modalOpen}
                onClose={() => this.setState({ modalOpen: false })}
                onSubmit={this.onSubmit}
                mountNode={this.buttonRef}
            />
        )
    }

    render() {

        return (
            <span
                ref={(ref) => this.buttonRef = ref}
            // href={proofreadURL}
            >
                <Button
                    primary
                    loading={this.props.loading}
                    disabled={this.props.loading}
                    onClick={() => this.setState({ modalOpen: true })}
                >
                    <Icon name="translate" /> Translate
                </Button>
                {this.renderAddHumanvoiceModal()}
            </span>
        )
    }
}


const mapStateToProps = ({ actionButton }) => ({
    organization: actionButton.organization,
    user: actionButton.user,
    video: actionButton.video,
    translatedArticles: actionButton.translatedArticles,
    loading: actionButton.loading,
})

const mapDispatchToProps = (dispatch) => ({
    generateTranslatableArticle: (translationURL, translationQueryParamKey, originalArticleId, langCode, langName) => dispatch(generateTranslatableArticle(translationURL, translationQueryParamKey, originalArticleId, langCode, langName)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Translate);