import React from 'react';
import { connect } from 'react-redux';

import * as actions from './modules/actions';
import ActionButton from './ActionButton';
import { withStyles } from 'direflow-component';
import styles from './style.scss';

class ActionButtonIndex extends React.Component {

    state = {
    }

    componentWillMount = () => {
        this.props.fetchUserData();
        this.props.fetchOrganizationData(this.props.apiKey);
    }

    render() {
        const { user, organization, video } = this.props;

        if (!user || !organization) return <span>Loading...</span>
        return <ActionButton {...this.props} />
    }
}

const mapStateToProps = ({ actionButton }) => ({
    organization: actionButton.organization,
    user: actionButton.user,
})

const mapDispatchToProps = (dispatch) => ({
    fetchUserData: () => dispatch(actions.fetchUserData()),
    fetchOrganizationData: apiKey => dispatch(actions.fetchOrganizationData(apiKey)),
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ActionButtonIndex))
