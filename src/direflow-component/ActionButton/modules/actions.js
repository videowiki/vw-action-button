import * as actionTypes from './types';
import Api from '../api';
import requestAgent from '../utils/requestAgent';

import NotificationService from '../utils/NotificationService';
import { getArticleTranslationURL } from '../utils/helpers';

const moduleName = 'actionButton';
let FETCH_VIDEO_INTERVAL_ID;

const setLoading = loading => ({
    type: actionTypes.SET_LOADING,
    payload: loading,
})

const setVideo = video => ({
    type: actionTypes.SET_VIDEO,
    payload: video,
})

const setUserData = user => ({
    type: actionTypes.SET_USER_DATA,
    payload: user,
})

const setOrganizationData = organization => ({
    type: actionTypes.SET_ORGANIZATION_DATA,
    payload: organization,
})

export const setUploadVideoModalOpen = (open) => ({
    type: actionTypes.SET_UPLOAD_VIDEO_MODAL_OPEN,
    payload: open,
})
export const setUploadVideoForm = (form) => ({
    type: actionTypes.SET_UPLOAD_VIDEO_FORM,
    payload: form
})

export const fetchUserData = () => dispatch => {
    requestAgent
        .get(Api.user.getUserDetails())
        .then(({ body }) => {
            dispatch(setUserData(body));
        })
        .catch(err => {
            console.log('error getting user data', err);
        })
}

export const fetchOrganizationData = (apiKey) => dispatch => {
    requestAgent
        .get(Api.apiKeys.getByKey(apiKey))
        .then(({ body }) => {
            console.log('============ got organization data', body)
            dispatch(setOrganizationData(body.apiKey.organization))
        })
        .catch(err => {
            console.log('error getting organization data', err);
        })
}

export const fetchVideoByTitle = ({ title, organization }) => (dispatch, getState) => {
    console.log('title', title, organization)
    requestAgent
        .get(Api.video.getVideos({ title, organization: organization }))
        .then(({ body }) => {

            console.log('============ got organization data', body)
            const { videos } = body;
            if (videos && videos[0]) {
                dispatch(setVideo(videos[0]));
                // If the video is in done status, fetch the video translations
                if (videos[0].status === 'done') {
                    dispatch(fetchVideoTranslations(videos[0]._id));
                }
            }
        })
        .catch(err => {
            console.log('error getting organization data', err);
        })
}

export const uploadAndTranscribeVideo = (form) => (dispatch, getState) => {
    const req = requestAgent
        .post(Api.video.uploadVideo(), form)
    dispatch(setLoading(true));

    req.then(({ body }) => {
        const video = body;
        if (video) {
            requestAgent.post(Api.video.transcribeVideo(video._id))
                .then(() => {
                    dispatch(setLoading(false));
                    dispatch(setUploadVideoModalOpen(false));
                    dispatch(fetchVideoByTitle({ title: video.title, organization: video.organization }));
                })
                .catch(err => {
                    console.log(err);
                })
        } else {
            dispatch(setLoading(false))
        }
    })
        .catch(err => {
            console.log('error getting organization data', err);
            NotificationService.responseError(err);
        })
}

const setTranslatedArticles = articles => ({
    type: actionTypes.SET_TRANSLATED_ARTICLES,
    payload: articles,
})

export const fetchVideoTranslations = (videoId) => (dispatch, getState) => {
    dispatch(setLoading(true));
    dispatch(setTranslatedArticles(null))
    const { organization } = getState()[moduleName];

    requestAgent
        .get(Api.article.getTranslatedArticles({ organization: organization._id, _id: videoId }))
        .then((res) => {
            const { videos } = res.body;
            dispatch(setTranslatedArticles(videos[0].articles))
            dispatch(setLoading(false))
        })
        .catch((err) => {
            NotificationService.responseError(err);
            dispatch(setLoading(false))
        })
}


export const generateTranslatableArticle = (translationURL, translationQueryParamKey, originalArticleId, langCode, langName, translators = [], verifiers = []) => (dispatch) => {
    const params = {}

    if (langCode) {
        const langCodeParts = langCode.split('-');
        if (langCodeParts.length === 1) {
            params.lang = langCode;
        } else {
            params.lang = langCodeParts[0];
            if (langCodeParts[1] === 'tts') {
                params.tts = true;
            }
        }
    }
    if (langName) {
        params.langName = langName;
    }
    let createdArtcile;
    requestAgent
        .post(Api.translate.generateTranslatableArticle(originalArticleId), params)
        .then((res) => {
            const { article } = res.body;
            createdArtcile = article;
            // Update translators if any
            return new Promise((resolve, reject) => {

                if (!translators || translators.length === 0) {
                    return resolve();
                }
                requestAgent
                    .put(Api.article.updateTranslators(article._id), { translators })
                    .then((res) => {
                        return resolve();
                    })
                    .catch((err) => {
                        NotificationService.responseError(err);
                        return resolve();
                    })
            })
        })
        .then(() => {
            // Update verifiers if any
            return new Promise((resolve) => {
                if (!verifiers || verifiers.length === 0) {
                    return resolve();
                }
                requestAgent
                    .put(Api.article.updateVerifiers(createdArtcile._id), { verifiers })
                    .then((res) => {
                        return resolve();
                    })
                    .catch((err) => {
                        console.log('error updating verifiers', err);
                        return resolve();
                    })
            })
        })
        .then(() => {
            if (createdArtcile) {
                const href = getArticleTranslationURL(translationURL, translationQueryParamKey, createdArtcile._id);
                window.location.href = href;
            }
        })
        .catch((err) => {
            console.log(err);
            NotificationService.responseError(err);
        })
}
