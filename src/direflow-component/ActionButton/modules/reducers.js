import * as actionTypes from './types';
const DEFAULT_LANG_CODE = 'en-US';

const INITIAL_STATE = {
    user: null,
    organization: null,
    loading: false,
    video: null,
    uploadVideoForm: {
        numberOfSpeakers: 1,
        langCode: DEFAULT_LANG_CODE,
    },
    uploadVideoModalOpen: false,
    translatedArticles: [],
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case actionTypes.SET_USER_DATA:
            return { ...state, user: action.payload };
        case actionTypes.SET_ORGANIZATION_DATA:
            return { ...state, organization: action.payload };
        case actionTypes.SET_LOADING:
            return { ...state, loading: action.payload };
        case actionTypes.SET_VIDEO:
            return { ...state, video: action.payload };
        case actionTypes.SET_UPLOAD_VIDEO_FORM:
            return { ...state, uploadVideoForm: action.payload };
        case actionTypes.SET_UPLOAD_VIDEO_MODAL_OPEN:
            return { ...state, uploadVideoModalOpen: action.payload };
        case actionTypes.SET_TRANSLATED_ARTICLES:
            return { ...state, translatedArticles: action.payload };
        default:
            return state;
    }
}