import { combineReducers } from 'redux'
import actionButton from './ActionButton/modules/reducers';

export default function createRootReducer (additionalReducers = {}) {
  const reducers = {
    actionButton,
  }

  return combineReducers(Object.assign({}, additionalReducers, reducers))
}
