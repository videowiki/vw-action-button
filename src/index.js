import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import 'abortcontroller-polyfill/dist/abortcontroller-polyfill-only';
import 'proxy-polyfill';
import { DireflowComponent } from 'direflow-component';
import App from './direflow-component/App';

const direflowComponent = new DireflowComponent();

const direflowProperties = {
  apiKey: '',
  apiRoot: '',
  
  videoTitle: '',
  videoURL: '',

  proofreadURL: '',
  proofreadQueryParamKey: 'video',

  translationURL: '',
  translationQueryParamKey: 'article',
};

const direflowPlugins = [
  // {
  //   name: 'font-loader',
  //   options: {
  //     google: {
  //       families: ['Advent Pro', 'Noto Sans JP'],
  //     },
  //   },
  // },
];

direflowComponent.configure({
  name: 'vw-action-button',
  // useShadow: true,
  properties: direflowProperties,
  plugins: direflowPlugins,
});

direflowComponent.create(App);
